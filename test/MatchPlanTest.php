<?php declare(strict_types=1);
namespace Tests;

use PHPUnit\Framework\TestCase;

final class MatchPlanTest extends TestCase
{
    public function testMatchCreationAtValidDate(): void
    {
        // GIVEN
        $date = '10/12/2021';
        $teamAgenda = new TeamAgenda();
        $match = new Match($date);
        // WHEN
        $teamAgenda->createMatch($match);
        // THEN événement existe à date match dans agenda
        $this->assertTrue(
            $teamAgenda->matchExist($date)
        );
    }
}